# Java sample application

## About this app

This Java application is generated from Spring Initializ (https://start.spring.io/).

Version:

```text
Spring Boot Version: 2.5.4
Packaging: Jar
Java version: 11
```

It use Spring Web as dependencies.

The application is running on port 8080 with simple Hello world text as response.

## Use it with Docker

### Docker Arguments

The provided Dockerfile supports the following arguments:

- JAR_NAME
- MAVEN_VERSION
- JRE_VERSION
- MVN_ARG (default is -DskipTests)
- MVN_CLEAN_ARG (either "true" or "false")

Please see more at Dockerfile for details.

### Usage

#### 1. Build image

Quick start command:

```bash
docker build  -t java:clean .
# mvn clean package -DskipTests
```

Specify clean cycle before packing the Java app:

```bash
docker build  -t java:clean --build-arg MVN_CLEAN_ARG=true .
# mvn clean package <MVN_ARG>
```

Skip clean cycle before packing the Java app:

```bash
docker build  -t java:clean --build-arg MVN_CLEAN_ARG=false .
# mvn package <MVN_ARG>
```

By default, we don't run test. But this can be overrided by let ```MVN_ARG=""```

```bash
docker build  -t java:clean --build-arg MVN_ARG="" .
```

#### 2. Run container

The application is running port 8080 in the container. To run application at port 80 on Docker host.

```bash
docker container run --rm -p 80:8080 java:clean 
```

## References

- http://whitfin.io/speeding-up-maven-docker-builds/
- https://snyk.io/blog/best-practices-to-build-java-containers-with-docker/
- https://github.com/Yelp/dumb-init
